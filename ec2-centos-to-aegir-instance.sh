#!/bin/sh
#
#   ec2-centos-to-aegir-instance
#   Transform a minimal CentOS 7 EC2 instance into a Aegir-controllable web server.
#
#   Copyright (c) 2015 James An <james@jamesan.ca>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.

declare -r TEMPFILE="$(mktemp)"

source ./utility-library

main() {
  _msg 'Adding custom repo and upgrading to latest + base + base-devel + LEMP stack - multilib...'
  #~ grep 'jan-webdev-x86_64' /etc/pacman.conf || \
      #~ sed --in-place 's/\(\[ec2\]\)/[jan-webdev-x86_64]\nServer = https:\/\/s3.amazonaws.com\/jan-webdev\/repo\n\n\1/' /etc/pacman.conf
  #~ pacman-key --recv-keys 001BED01
  #~ pacman-key --lsign-key 001BED01
  yum --assumeyes upgrade
  yum --assumeyes install epel-release
  rpm --upgrade --hash -v \
      ttp://www.elrepo.org/elrepo-release-7.0-2.el7.elrepo.noarch.rpm \
      http://dl.iuscommunity.org/pub/ius/stable/CentOS/7/x86_64/ius-release-1.0-13.ius.centos7.noarch.rpm \
      http://nginx.org/packages/centos/7/noarch/RPMS/nginx-release-centos-7-0.el7.ngx.noarch.rpm
  #~ echo \
#~ "[mariadb]
#~ name = MariaDB
#~ baseurl = http://yum.mariadb.org/10.0/centos7-amd64
#~ gpgkey=https://yum.mariadb.org/RPM-GPG-KEY-MariaDB
#~ gpgcheck=1" | sudo tee /etc/yum.repos.d/mariadb.repo
  #~ yum-config-manager --enable centosplus --enable mariadb --enable ius-testing
  yum-config-manager --enable centosplus --enable ius-testing
  yum --assumeyes install yum-{plugin-{aliases,changelog,downloadonly,fastestmirror,filter-data,fs-snapshot,keys,list-data,merge-conf,priorities,protectbase,ps,remove-with-leaves,replace,rpm-warm-cache,security,show-leaves,tsflags,verify,versionlock},presto,utils}
  yum --assumeyes install bash-completion libpwquality mlocate moreutils

  #~ pacman --noconfirm --sync drush-git mariadb msmtp-mta nginx php-fpm
  #~ pacman --noconfirm --sync aegir
  yum --assumeyes install mariadb-client mariadb-server msmtp nginx php-fpm
  # msmtp-mta
  ln -sr /usr/bin/msmtp /usr/bin/sendmail
  # aegir user
  if ! getent passwd aegir &>/dev/null; then
      useradd --non-unique --uid $(id --user apache) --gid $(id --group apache) --home-dir /var/lib/aegir aegir
      # Rearrange user database so aegir user name comes before http
      cat >| $TEMPFILE \
          <(sed -n '/apache:/q;p' /etc/passwd) \
          <(tail -n1 /etc/passwd) \
          <(sed -n '/apache:/{p;q}' /etc/passwd) \
          <(head -n-1 /etc/passwd | sed '1,/apache:/d')
      cp $TEMPFILE /etc/passwd
      pwconv
  fi

  package-cleanup --quiet --leaves --exclude-bin | grep -v yum | xargs yum remove --assumeyes && yum clean all && yum makecache

  _msg 'Setting server properties...'
  hostnamectl set-hostname $(curl http://169.254.169.254/latest/meta-data/public-hostname)
  timedatectl set-timezone America/Toronto
  timedatectl set-ntp FALSE
  setenforce Permissive

  _msg 'Configure outgoing mail service...'
  echo \
'# Accounts will inherit settings from this section
defaults
auth            on
tls             on
tls_trust_file  /etc/ssl/certs/ca-certificates.crt

account         default
host            smtp.gmail.com
port            587
from            temp@jamesan.ca
user            temp@jamesan.ca
password        1234asDF%' \
      > "$TEMPFILE"
  install --mode=644 "$TEMPFILE" /etc/msmtprc

  _msg 'Setting up user skeleton directory with GnuPG and SSH...'
  install --mode=644 -D   /usr/share/doc/gnupg/examples/gpgconf.conf /etc/gnupg/gpgconf.conf
  install --mode=700 --directory /etc/skel/.gnupg
  install --mode=700 --directory /etc/skel/.ssh
  install --mode=600 -D   /usr/share/gnupg/gpg-conf.skel /etc/skel/.gnupg/gpg.conf
  install --mode=600 "$TEMPFILE" /etc/skel/.gnupg/gpg-agent.conf
  install --mode=600 "$TEMPFILE" /etc/skel/.gnupg/pubring.gpg
  install --mode=600 "$TEMPFILE" /etc/skel/.gnupg/secring.gpg
  install --mode=600 "$TEMPFILE" /etc/skel/.ssh/authorized_keys

# TODO!!

  _msg 'Generate per-user MySQL credentials and disable all shell password authentication...'
  systemctl start mariadb
  SQL_CMDS=()
  for USERNAME in aegir centos root; do
      pwmake 2048 | sudo -u $USERNAME tee $(eval echo ~$USERNAME/.sql-passwd) &>/dev/null
      passwd --lock $USERNAME
      SQL_CMDS+=("GRANT ALL PRIVILEGES ON *.* TO '$USERNAME'@'%' IDENTIFIED BY '$(cat $(eval echo ~$USERNAME/.sql-passwd))' WITH GRANT OPTION;")
  done
  unset SQL_CMDS[${#SQL_CMDS[@]}-1]
  SQL_CMDS+=(
      "UPDATE mysql.user SET PASSWORD=PASSWORD('$(cat /root/.sql-passwd)') WHERE User='root';"
      "FLUSH PRIVILEGES;"
  )
  mysql --user=root --execute="echo ${SQL_CMDS[@]}"

  _msg 'Tightening login setting for the SSH daemon...'
  sed --regexp-extended --in-place 's/^#(LoginGraceTime) .*$/\1 30s/' /etc/ssh/sshd_config
  sed --regexp-extended --in-place 's/^#(PermitRootLogin) .*$/\1 no/' /etc/ssh/sshd_config
  sed --regexp-extended --in-place 's/^#(MaxAuthTries) .*$/\1 3/' /etc/ssh/sshd_config
  sed --regexp-extended --in-place 's/^#(MaxSessions) .*$/\1 5/' /etc/ssh/sshd_config
  sed --regexp-extended --in-place 's/^#(UseDNS) .*$/\1 no/' /etc/ssh/sshd_config

  _msg 'Setting up LEMP stack as dedicated Drupal host and Aegir controller'
  mysql --user=root --password=$(cat ~/.sql-passwd) --execute="
      DELETE FROM mysql.user WHERE User='';
      DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1');
      DROP DATABASE IF EXISTS test;
      DELETE FROM mysql.db WHERE Db='test' OR Db='test\\_%';
      FLUSH PRIVILEGES;"
  su aegir --command "drush hostmaster-install --yes --web_group=http --http_service_type=nginx \
      --root=/var/lib/aegir/hostmaster --aegir_db_user=aegir --aegir_db_pass=$(cat ~aegir/.sql-passwd) \
      --aegir_host=$(hostname) --client_email=temp@jamesan.ca $(hostname)"
  su aegir --command "sed --in-place 's/127.0.0.1:9000/unix:\\/run\\/php-fpm\\/php-fpm.sock/' /var/lib/aegir/config/includes/nginx_vhost_common.conf"
  su aegir --command "drush @hostmaster pm-enable --yes admin_devel admin_menu betterlogin entity entity_token file_entity hosting_queued libraries module_filter pathauto token"
  su aegir --command "drush @hostmaster pm-update --yes"

  _msg 'Starting web stack and enabling it at boot.'
  systemctl stop aegir.service aegir.target mysqld.service nginx.service php-fpm.service
  systemctl start aegir.target && systemctl enable aegir.target

  _msg 'Set up complete!'
  reboot
}

# invoke main function if this script is executed as a command
if [ "$CMD" = ${BASH_SOURCE##*/} ]; then
  main "$@"
# otherwise remo    ve main function
else
  unset main
fi
