SCRIPT_PATH=~/Documents/GitHub/drupal-ec2-scripts
SCRIPT_LIBS=utility-library
SCRIPT_ARCH=ec2-arch-to-aegir-controller.sh
SCRIPT_ARC2=ec2-arch-to-aegir-controller.sh
SCRIPT_RHEL=ec2-centos-to-aegir-instance.sh

#~ IP1_ID=eipalloc-3478f051
#~ IP1_ADDR=54.152.154.199
IP1_ID=eipalloc-79318a1c
IP1_ADDR=54.174.185.245
IP1_DNS=ec2-${IP1_ADDR//./-}.compute-1.amazonaws.com
#~ IP2_ID=eipalloc-2c78f049
#~ IP2_ADDR=52.1.126.42
#~ IP2_DNS=ec2-${IP2_ADDR//./-}.compute-1.amazonaws.com


AMI_ARCH_LINUX=ami-fcfdb394
#~ AMI_CENTOS_7=ami-96a818fe
#~ GROUP_ID=sg-41c66a25
GROUP_ID=sg-cfae53ab
#~ KEY_NAME=james@jamesan.ca
KEY_NAME=nestle-ca-dev
INST_TYPE=t2.micro
AV_ZONE=us-east-1a

CTRL_INST="$(ec2-run-instances $AMI_ARCH_LINUX \
  --group $GROUP_ID \
  --key $KEY_NAME \
  --instance-type $INST_TYPE \
  --availability-zone $AV_ZONE \
  --associate-public-ip-address false | grep INSTANCE | cut -f2)"
#~ NODE_INST="$(ec2-run-instances $AMI_CENTOS_7 \
  #~ --group $GROUP_ID \
  #~ --key $KEY_NAME \
  #~ --instance-type $INST_TYPE \
  #~ --availability-zone $AV_ZONE \
  #~ --associate-public-ip-address false | grep INSTANCE | cut -f2)"
while test $(ec2-describe-instance-status $CTRL_INST | grep passed | wc -l | head -c-1) -lt 2; do
  sleep 10
done
ec2-associate-address --instance $CTRL_INST --allocation-id $IP1_ID &>/dev/null
#~ ec2-associate-address --instance $NODE_INST --allocation-id $IP2_ID &>/dev/null

HOST_INFO="$(ec2-describe-instances $CTRL_INST)"
HOST_KEYS="$(ec2-get-console-output $CTRL_INST | sed '1,/BEGIN SSH HOST KEY FINGERPRINTS/d;/END/Q' | cut -f3,4 -d' ')"
HOST_ECDSA_KEY=$(echo "$HOST_KEYS" | grep ecdsa | cut -f1 -d' ')

truncate --no-create --size=0 ~/.ssh/known_hosts
chmod +x $SCRIPT_PATH/$SCRIPT_ARCH $SCRIPT_PATH/$SCRIPT_ARC2 $SCRIPT_PATH/$SCRIPT_RHEL

echo $HOST_ECDSA_KEY
ssh root@$IP1_DNS exit
scp $SCRIPT_PATH/$SCRIPT_ARCH $SCRIPT_PATH/$SCRIPT_ARC2 $SCRIPT_PATH/$SCRIPT_LIBS root@$IP1_DNS:~
ssh root@$IP1_DNS "~/$SCRIPT_ARCH"


# Move aegir line above http in /etc/passwd
# Change http user to aegir in /etc/sudoers.d/aegir

for INSTANCE in $(ec2-describe-instances | grep INSTANCE | grep -v terminated | cut -f2 | xargs echo); do
  _msg "Terminating AWS EC2 instance, $CTRL_INST."
  ec2-terminate-instances $INSTANCE
done

