#!/bin/sh
#
#   ec2-arch-to-aegir-controller
#   Transform a base Arch Linux EC2 instance into a Aegir controller.
#
#   Copyright (c) 2015 James An <james@jamesan.ca>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.

declare -r TEMPFILE="$(mktemp)"
declare -r EMAIL='temp@jamesan.ca'
declare -r PASSWD='1234asDF%'

source ./utility-library

main() {
  _msg 'Adding custom repo and upgrading to latest + base + base-devel + LEMP stack - multilib...'
  grep 'jan-webdev-x86_64' /etc/pacman.conf || \
      sed --in-place 's/\(\[ec2\]\)/[jan-webdev-x86_64]\nServer = https:\/\/s3.amazonaws.com\/jan-webdev\/repo\n\n\1/' /etc/pacman.conf
  pacman-key --recv-keys 001BED01
  pacman-key --lsign-key 001BED01
  pacman --noconfirm --sync --refresh --sysupgrade --needed base base-devel
  yes | pacman --sync bash-completion gcc gcc-libs git libpwquality mlocate moreutils rsync unzip wget zip
  pacman --query --quiet --search 32 | xargs pacman --noconfirm --remove --nosave --recursive linux
  pacman --noconfirm --sync drush-git mariadb msmtp-mta nginx php-fpm
  pacman --noconfirm --sync aegir

  _msg 'Setting server properties...'
  hostnamectl set-hostname $(curl http://169.254.169.254/latest/meta-data/public-hostname)
  timedatectl set-timezone America/Toronto
  timedatectl set-ntp FALSE
  sed "s/address@gmail.com/$EMAIL/;s/yourpassword/$PASSWD/" /etc/msmtprc.aegir > /etc/msmtprc

  _msg 'Setting up user skeleton directory with GnuPG and SSH...'
  truncate -s0 "$TEMPFILE"
  install --mode=644 -D   /usr/share/doc/gnupg/examples/gpgconf.conf /etc/gnupg/gpgconf.conf
  install --mode=700 --directory /etc/skel/.gnupg
  install --mode=700 --directory /etc/skel/.ssh
  install --mode=600 -D   /usr/share/gnupg/gpg-conf.skel /etc/skel/.gnupg/gpg.conf
  install --mode=600 "$TEMPFILE" /etc/skel/.gnupg/gpg-agent.conf
  install --mode=600 "$TEMPFILE" /etc/skel/.gnupg/pubring.gpg
  install --mode=600 "$TEMPFILE" /etc/skel/.gnupg/secring.gpg
  install --mode=600 "$TEMPFILE" /etc/skel/.ssh/authorized_keys

  _msg 'Disabling remote root login and creating remote-accessible non-root user...'
  truncate --size=0 /root/.ssh/authorized_keys
  useradd --create-home --no-user-group --gid users cloud-user
  curl http://169.254.169.254/latest/meta-data/public-keys/0/openssh-key >| "$TEMPFILE"
  install --mode=600 -D --owner=cloud-user --group=users "$TEMPFILE" ~cloud-user/.ssh/authorized_keys
  install --mode=600 -D --owner=cloud-user --group=users "$TEMPFILE" ~aegir/.ssh/authorized_keys
  echo 'cloud-user ALL=(ALL) NOPASSWD: ALL' >| "$TEMPFILE"
  install --mode=440 "$TEMPFILE" /etc/sudoers.d/cloud-user

  _msg 'Tightening login setting for the SSH daemon...'
  sed --regexp-extended --in-place 's/^#(LoginGraceTime) .*$/\1 30s/' /etc/ssh/sshd_config
  sed --regexp-extended --in-place 's/^#(PermitRootLogin) .*$/\1 no/' /etc/ssh/sshd_config
  sed --regexp-extended --in-place 's/^#(MaxAuthTries) .*$/\1 3/' /etc/ssh/sshd_config
  sed --regexp-extended --in-place 's/^#(MaxSessions) .*$/\1 5/' /etc/ssh/sshd_config
  sed --regexp-extended --in-place 's/^#(UseDNS) .*$/\1 no/' /etc/ssh/sshd_config

  _msg 'Generate per-user MySQL credentials and disable all shell password authentication...'
  systemctl start mysqld.service
  SQL_CMDS=()
  for USERNAME in aegir cloud-user root; do
      pwmake 2048 | sudo -u $USERNAME tee $(eval echo ~$USERNAME/.sql-passwd) &>/dev/null
      passwd --lock $USERNAME
      SQL_CMDS+=("GRANT ALL PRIVILEGES ON *.* TO '$USERNAME'@'%' IDENTIFIED BY '$(cat $(eval echo ~$USERNAME/.sql-passwd))' WITH GRANT OPTION;")
  done
  test -v SQL_CMDS && unset SQL_CMDS[${#SQL_CMDS[@]}-1]
  SQL_CMDS+=(
      "UPDATE mysql.user SET PASSWORD=PASSWORD('$(cat ~root/.sql-passwd)') WHERE User='root';"
      "DELETE FROM mysql.user WHERE User='';"
      "DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1');"
      "DROP DATABASE IF EXISTS test;"
      "DELETE FROM mysql.db WHERE Db='test' OR Db='test\\_%';"
      "FLUSH PRIVILEGES;"
  )
  mysql --user=root --execute="${SQL_CMDS[*]}"

  _msg 'Setting up LEMP stack as dedicated Drupal host and Aegir controller'
  su aegir --command=" \\
drush hostmaster-install --yes $(hostname) \\
--aegir_db_user=aegir --aegir_db_pass=$(cat ~aegir/.sql-passwd) \\
--aegir_host=$(hostname) --client_email=temp@jamesan.ca \\
--http_service_type=nginx --root=/var/lib/aegir/hostmaster --web_group=http"
  su aegir --command="drush @hostmaster pm-update --yes"
  sed -i 's#127.0.0.1:9000#unix:/run/php-fpm/php-fpm.sock#' /var/lib/aegir/config/includes/nginx_vhost_common.conf

  _msg 'Starting web stack and enabling it at boot.'
  systemctl daemon-reload
  systemctl stop mysqld.service
  systemctl enable aegir.target

  _msg 'Set up complete!'
}

# invoke main function if this script is executed as a command
if [ "$CMD" = ${BASH_SOURCE##*/} ]; then
  main "$@"
else
  unset main
fi
