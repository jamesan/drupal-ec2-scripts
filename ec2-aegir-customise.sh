#!/bin/sh
#
#   ec2-arch-to-aegir-controller
#   Transform a base Arch Linux EC2 instance into a Aegir controller.
#
#   Copyright (c) 2015 James An <james@jamesan.ca>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.

declare -r TEMPFILE="$(mktemp)"
declare -r URL=dev.aegir.nestle.onemethod.com

# A=0; cat /tmp/list | sed 's/\s\s*/\n/' | while read PRJT; do A=$((A+1)); printf "%-25s" $PRJT; if [ $A -eq 5 ]; then A=0; printf "\n"; fi; done | sed 's/\s
declare -a PROJECTS=(
  addanother               admin_language           admin_menu               admin_views              adminimal_admin_menu
  adminimal_theme          advanced_help            apachesolr               apachesolr_attachments   apachesolr_autocomplete
  apachesolr_confgen       apachesolr_multilingual  apachesolr_sort          apachesolr_views         better_exposed_filters
  betterlogin              breakpoints              bundle_copy              color_field              ctools
  date                     deploy                   devel                    devel_themer             disable_messages
  draggableviews           entity                   entity_dependency        entity_operations        entity_translation
  entity_view_mode         entityreference          fast_404                 features                 feeds
  fences                   field_collection         field_group              file_entity              git_deploy
  i18n                     i18n_404                 i18n_contrib             i18n_hide_sync           i18n_media
  i18nviews                imce                     imce_wysiwyg             inline_entity_form       jammer
  job_scheduler            jquery_plugin            jquery_update            lang_dropdown            languageicons
  libraries                link                     media                    media_translation        metatag
  module_filter            options_element          pagerer                  password_policy          path_alias_xt
  pathauto                 pathauto_i18n_taxonomy   pathologic               plupload                 querypath
  remote_stream_wrapper    rubik                    rules                    scheduler                schema
  services                 strongarm                subpathauto              tao                      taxonomy_csv
  title                    token                    token_filter             token_insert             transliteration
  uuid                     uuid_features            variable                 vars                     viewfield
  views                    views_bulk_operations    views_field_view         views_fieldset           views_slideshow
  views_slideshow_xtra     wysiwyg
)
declare -a EXTENSIONS=(
  entity_dependency        entity_operations        entity_token
  entity_view_mode         entityreference          fast_404                 features                 feeds
  feeds_ui                 fences                   field_collection         field_group              field_ui
  file                     file_entity              git_deploy               help                     i18n
  i18n_404                 i18n_field               i18n_hreflang            i18n_media               i18n_menu
  i18n_path                i18n_string              i18n_taxonomy            i18n_variable            i18nviews
  image                    imce                     imce_wysiwyg             inline_entity_form
  jquery_update            lang_dropdown            languageicons            libraries                link
  list                     locale                   media                    media_internet           menu
  module_filter            number                   options_element          pagerer                  password_policy
  password_policy_password_tab  path                path_alias_xt            pathauto                 pathauto_i18n_taxonomy
  pathologic               plupload                 remote_stream_wrapper    scheduler                schema
  strongarm                subpathauto              taxonomy                 taxonomy_csv             title
  token                    transliteration          uuid                     uuid_features            variable
  variable_admin           variable_realm           variable_store           variable_views           vars
  views                    views_bulk_operations    views_field_view         views_fieldset           views_slideshow
  views_slideshow_xtra     views_ui                 wysiwyg
)
declare -a DRUSH_PROJECTS=(
  backup_migrate      coder               coder_review        delete_all
  devel               drush_ctex_bonus    drush_entity        hacked
  libraries           module_builder      schema              site_audit
  state
)
declare -a HOST_FEATS=(alias clone cron migrate platform_pathauto queued task_gc)
declare -A HOST_BOOL_VARS=(
)
declare -A HOST_INT_VARS=(
  [hosting_queue_task_gc_enabled]=1
  [hosting_queue_task_gc_frequency]=3600
  [hosting_require_disable_before_delete]=0
)
declare -A HOST_JSON_VARS=(
)
declare -A HOST_STR_VARS=(
  [hosting_default_profile]=minimal
  [hosting_platform_pathauto_base_path]=/var/lib/aegir/platforms/
)
declare -A SITE_BOOL_VARS=(
)
declare -A SITE_INT_VARS=(
  [admin_menu_cache_client]=0
  [admin_menu_tweak_tabs]=1
  [configurable_timezones]=0
  [date_first_day]=0
  [user_default_timezone]=0
)
declare -A SITE_JSON_VARS=(
)
declare -A SITE_STR_VARS=(
  [date_default_timezone]=America/Toronto
  [site_default_country]=CA
)
declare -A VAR_TYPES=(
  [BOOL]=boolean
  [INT]=integer
  [JSON]=json
  [STR]=string
)

source ./utility-library

main() {
  systemctl start aegir.target
  _msg 'Configuring hostmaster front-end...'
  for HOST_FEAT in "${HOST_FEATS[@]}"; do
      sudo --user=aegir drush @hostmaster pm-enable --yes hosting_$HOST_FEAT
      HOST_INT_VARS+=([hosting_feature_$HOST_FEAT]=1)
  done
  for TYPE in "${!VAR_TYPES[@]}"; do
      declare FORMAT="${VAR_TYPES[$TYPE]}"
      declare -n HOST_VARS="HOST_${TYPE}_VARS"
      declare -n SITE_VARS="SITE_${TYPE}_VARS"
      for KEY in "${!HOST_VARS[@]}"; do
          declare VAL="${HOST_VARS[$KEY]}"
          sudo --user=aegir drush @hostmaster vset --exact --format=$FORMAT $KEY $VAL
      done
      for KEY in "${!SITE_VARS[@]}"; do
          declare VAL="${SITE_VARS[$KEY]}"
          sudo --user=aegir drush @hostmaster vset --exact --format=$FORMAT $KEY $VAL
      done
  done
  sudo --user=aegir git -C $(sudo --user=aegir drush @hostmaster drupal-directory)/sites/all/modules clone --branch 7.x-1.x http://git.drupal.org/sandbox/jamesan/2442039.git hostmaster_tweaks
  sudo --user=aegir drush @hostmaster pm-enable --yes hostmaster_tweaks
  sudo --user=aegir drush pm-download --yes --destination=~aegir/.drush "${DRUSH_PROJECTS[@]}"

  _msg 'Installing latest, supported, and recommended version of Drupal as the main "drupal" platform...'
  sudo --user=aegir drush @hostmaster pm-download --yes --destination=/var/lib/aegir/platforms --drupal-project-rename drupal
  sudo --user=aegir drush provision-save @platform_Drupal \
      --context_type=platform --root=/var/lib/aegir/platforms/drupal \
      --server=@server_master --web_server=@server_master
  sudo --user=aegir drush @platform_Drupal provision-verify
  sudo --user=aegir drush @hostmaster hosting-import @platform_Drupal

  _msg 'Installing a Drupal site...'
  sudo --user=aegir drush provision-save @$URL \
      --context_type=site --client_name=admin --db_server=@server_localhost \
      --language=en --platform=platform_Drupal --profile=minimal --uri=$URL
  sudo --user=aegir drush @hostmaster hosting-import @$URL

  _msg 'Setting up site folder hierarchy...'
  sudo --user=aegir mkdir --verbose $(sudo --user=aegir drush @$URL drupal-directory modules)/contrib
  sudo --user=aegir mkdir --verbose $(sudo --user=aegir drush @$URL drupal-directory modules)/custom
  sudo --user=aegir mkdir --verbose $(sudo --user=aegir drush @$URL drupal-directory modules)/features
  sudo --user=aegir mkdir --verbose $(sudo --user=aegir drush @$URL drupal-directory themes)/contrib
  sudo --user=aegir mkdir --verbose $(sudo --user=aegir drush @$URL drupal-directory themes)/custom
  sudo --user=aegir drush @$URL pm-download --use-site-dir "${PROJECTS[@]}"
  sudo --user=aegir --set-home git -C $(sudo --user=aegir drush @$URL drupal-directory themes)/custom clone --branch develop-drupal git@bitbucket.org:onemethod/nestle-ca-frontend.git nestle_ca

  _msg 'Enabling extensions and setting persistent variables...'
  sudo --user=aegir drush @$URL pm-enable --yes "${EXTENSIONS[@]}"
  for TYPE in "${!VAR_TYPES[@]}"; do
      declare FORMAT="${VAR_TYPES[$TYPE]}"
      declare -n SITE_VARS="SITE_${TYPE}_VARS"
      for KEY in "${!SITE_VARS[@]}"; do
          declare VAL="${SITE_VARS[$KEY]}"
          sudo --user=aegir drush @$URL vset --exact --format=$FORMAT $KEY $VAL
      done
  done

  _msg 'Setup complete! Verifying all sites, platforms, and servers...'
  for ALIAS in none self server_master server_localhost platform_hostmaster platform_Drupal hostmaster $URL; do
      sudo --user=aegir drush @hostmaster hosting-task @$ALIAS verify
  done
  sudo --user=aegir drush @hostmaster hosting-task_gc
}

# invoke main function if this script is executed as a command
if [ "$CMD" = ${BASH_SOURCE##*/} ]; then
  main "$@"
# otherwise remo    ve main function
else
  unset main
fi
